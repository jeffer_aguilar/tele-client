/**
 * Created by Jefferson on 21/7/2016.
 */

angular.module('lautcom',
    [
        'ngResource',
        'ui.router',
        'ngMaterial',
        'ngMessages',
        'ngLodash',
        'highcharts-ng',
        'duScroll'
    ])
    .constant('API_URL', 'https://sleepy-plains-13838.herokuapp.com')
    .config(['$mdThemingProvider', '$compileProvider', '$mdAriaProvider',
        function($mdThemingProvider, $compileProvider, $mdAriaProvider){
            $mdThemingProvider.theme('default')
                .primaryPalette('indigo');
            $compileProvider.debugInfoEnabled(false);
            $mdAriaProvider.disableWarnings();
        }]);

String.prototype.capitalize = function() {
    return this.charAt(0).toUpperCase() + this.slice(1);
};

