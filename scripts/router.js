'use strict';

angular.module('lautcom')
	.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider){
		$urlRouterProvider.otherwise('/modulacion');
		$stateProvider
		// HOME STATES ========================================
			.state('home', {
				url: '/',
				templateUrl: 'components/home/home.html',
				controller: 'HomeCtrl'
			})
			.state('home.modulacion', {
				url: 'modulacion',
				templateUrl: 'components/modulacion/index.html',
				controller: 'ModulacionCtrl'
			});
	}]);