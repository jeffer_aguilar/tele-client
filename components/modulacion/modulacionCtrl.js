/**
 * Created by Jefferson on 1/2/2017.
 */
angular.module('lautcom')
    .controller('ModulacionCtrl', ['$scope', '$mdDialog', '$http', 'API_URL', 'lodash', '$timeout', function ($scope, $mdDialog, $http, API_URL, lodash, $timeout) {
        $scope.modulacion = {
            moduladora: {
                modelo: 'cos',
                frecuencia: '',
                amplitud: ''
            },
            portadora: {
                modelo: 'cos',
                frecuencia: '',
                amplitud: ''
            },
            modulada: {},
            tipo: 'FM',
            sensibilidad: '',
            resistencia: ''
        };

        /*$scope.sensibilidad = {
            frecuencia: '',
            fase: ''
        };*/

        /*$scope.frecuenciaModuladora = {
            valor : 1000,
            unidad: {}
        };*/

        /*$scope.frecuenciaPortadora = {
            valor : 10000,
            unidad: {}
        };*/
        // $scope.frecuenciaPortadora = angular.copy($scope.frecuenciaModuladora);

        $scope.tituloMod = "frecuencia";

        $scope.signals = [
            {
                id: 'sen',
                nombre: "seno"
            },
            {
                id: 'cos',
                nombre: "coseno"
            },
            {
                id: 'diente_sierra',
                nombre: "diente de sierra"
            },
            {
                id: 'triangular',
                nombre: "triangular"
            }
        ];

        $scope.unidadesFrecuencia = [
            {
                nombre: "Hz",
                valor: 1
            },
            {
                nombre: "KHz",
                valor: 1000
            },
            {
                nombre: "MHz",
                valor: 1000000
            },
            {
                nombre: "GHz",
                valor: 1000000000
            }/*,
            {
                nombre: "THz",
                valor: 1000000000000
            },
            {
                nombre: "PHz",
                valor: 1000000000000000
            }*/
        ];

        $scope.chartGeneralConfig = {
            options: {
                chart: {
                    type: 'spline',
                    zoomType: 'x'/*,
                    panning: true,
                    panKey: 'shift'*/
                },
                loading: {
                    hideDuration: 1000,
                    showDuration: 1000,
                    labelStyle: {
                        // color: '#fff',
                        fontWeight: 'bold',
                        position: 'relative',
                        top: '45%',
                        fontSize:'30px'
                    }/*,
                    style: {
                        opacity: 0.5,
                        /!*backgroundRepeat: 'no-repeat',
                        backgroundSize: 'cover',*!/
                        position: 'absolute',
                        width: '100',
                        height: '100',
                        backgroundColor: '#000'
                    }*/
                },
                lang :{
                    loading:'Cargando señal...'
                    // loading:'<md-progress-circular md-mode="indeterminate"></md-progress-circular>'
                },
                tooltip: {
                    // shared: true,
                    crosshairs: true,
                    enabled: false,
                    formatter: function() {
                        return '<strong>Segundo:</strong> ' + this.x + 's; <br>' +
                                '<strong>Voltaje:</strong> '+ this.y + 'v';
                    }
                },
                plotOptions: {
                    spline: {
                        marker: {
                            enabled: false
                        }
                    }
                }
            },
            title: {
                text: ''
            },
            xAxis: {
                // labels: {
                //     enabled: false
                // },
                // lineColor: 'transparent',
                // minorTickLength: 0,
                // tickLength: 0,
                title: {
                    text: 'Tiempo (segundos)'
                },
                labels: {
                    formatter: function() {
                        var resul = this.value.toString().split(".");
                        if (resul.length > 1 && resul[1].length > 3)
                            return this.value.toExponential(1) + 's';
                        return this.value + 's';
                    }
                }
            },
            yAxis: {
                // tickInterval: 3,
                title: {
                    text: 'Amplitud (voltios)'
                },
                labels: {
                    format: '{value}v'
                }
            },
            credits: {
                enabled: false
            },
            series: [{
                showInLegend: false,
                data: [
                    [0,0],
                    [5,0]
                ]
            }],
            loading: false
        };

        $scope.espectrosChartConfig = {
            options: {
                chart : {type: "column"},
                plotOptions: 
                {
                    column: {
                        maxPointWidth: 15
                    }
                },
                loading: {
                    hideDuration: 1000,
                    showDuration: 1000,
                    labelStyle: {
                        // color: '#fff',
                        fontWeight: 'bold',
                        position: 'relative',
                        top: '45%',
                        fontSize:'30px'
                    }
                },
                tooltip: {
                    formatter: function() {
                        signo = this.point.polaridad ? "" : "-"
                        return '<strong>Frecuencia:</strong> ' + this.x + '<strong>Hz</strong> <br>' +
                                '<strong>Amplitud:</strong> '+signo + this.y + '<strong>V</strong> <br>'+
                                '<strong>Potencia:</strong> '+ this.point.potencia + '<strong>W</strong>';
                    }
                }
            },
            title: {
                text: ''
            },
            credits: {
                enabled: false
            },
            series : [{showInLegend: false, data: []}],
            yAxis: {
                // tickInterval: 3,
                title: {
                    text: 'Voltios'
                },
                labels: {
                    format: '{value}v'
                }
            },
            xAxis: {
                // tickInterval: 3,
                title: {
                    text: 'Frecuencia (Hz)'
                }
            }
            
        };

        $scope.moduladoraChartConfig = angular.copy($scope.chartGeneralConfig);

        $scope.portadoraChartConfig = angular.copy($scope.chartGeneralConfig);

        $scope.moduladaChartConfig = angular.copy($scope.chartGeneralConfig);
        $scope.moduladaChartConfig.series[0].data = [];

        $scope.demoduladaChartConfig = angular.copy($scope.chartGeneralConfig);
        $scope.demoduladaChartConfig.series[0].data = [];

        $scope.moduladaChartConfig.options.plotOptions = {
            series: {
                cursor: 'pointer',
                point: {
                    events: {
                        click: function (e) {
                            $scope.calcInstantanea(e.point.x, false);
                            $scope.calcInstantanea(e.point.x, true);
                            $timeout(function () {
                                hs.htmlExpand(null, {
                                    headingText: 'Valores instantáneos de ' +$scope.tituloMod,
                                    maincontentText: 'Tiempo: <br>' +e.point.x + 's <br><br>' +
                                    ''+$scope.tituloMod.capitalize()+' intantánea:<br>'+$scope.modulacion.modulada.fInst+' Hz<br><br>' +
                                    'Desviación intantánea de '+$scope.tituloMod+':<br>'+$scope.modulacion.modulada.desInst+' Hz',
                                    width: 300
                                });
                            }, 700);
                        }
                    }
                }
            },
            spline: {
                marker: {
                    enabled: false
                }
            }
        };

        $scope.cambioTipoModulacion = function () {
            if ($scope.modulacion.tipo == "PM")
                $scope.tituloMod = "fase";
            else
                $scope.tituloMod = "frecuencia";
        };

        /*$scope.cambioSenbilidad = function (tipo) {
            var wm = 2*Math.PI*$scope.modulacion.moduladora.frecuencia;
            if (tipo == "FM"){
                $scope.modulacion.sensibilidad = angular.copy($scope.sensibilidad.frecuencia);
                $scope.sensibilidad.fase = $scope.sensibilidad.frecuencia / wm;
            }
            else {
                $scope.modulacion.sensibilidad = angular.copy($scope.sensibilidad.fase);
                $scope.sensibilidad.frecuencia = $scope.sensibilidad.fase * wm;
            }
        };*/

        $scope.validarPeticion = function (formulario, modulada = false) {
            if ((formulario.$valid && formulario.$dirty)|| modulada) { // Un parchaso a un error mistico
                if ($scope.moduladoraForm.$valid && $scope.portadoraForm.$valid && ($scope.modularForm.$valid||modulada) &&
                    lodash.isNumber($scope.modulacion.sensibilidad) && $scope.modulacion.sensibilidad>0) {
                    // Si entra aquí mando a modular
                    modularSenal();
                }
                if (formulario.$name == 'moduladoraForm' || formulario.$name == 'portadoraForm' ) {
                    var data, senal, senalChart;
                    if (formulario.$name == 'moduladoraForm') {
                        senalChart = $scope.moduladoraChartConfig;
                        senal = $scope.modulacion.moduladora;
                        senal.tipo = "m";
                    }
                    else {
                        senalChart = $scope.portadoraChartConfig;
                        senal = $scope.modulacion.portadora;
                        senal.tipo = "p";
                    }

                    // Es inecesario este mapeo, luego lo cambio
                    data = {
                        amplitud: senal.amplitud,
                        frecuencia: senal.frecuencia,
                        modelo: senal.modelo,
                        tipo: senal.tipo,
                        ruido : $scope.ruido.val
                    };

                    getSenal(senalChart, data);
                }
                formulario.$setDirty();
                formulario.$setPristine();
            }
        };

        $scope.calculadoraDesviacion = function(ev) {
            $mdDialog.show({
                locals:{tipo: $scope.modulacion.tipo},
                controller: 'DesviacionDialogCtrl',
                templateUrl: 'dialogs/calcularDesviacion.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose:true,
                fullscreen: false
            })
                .then(function(res) {
                    /*if (tipo == "FM") $scope.sensibilidad.frecuencia = res;
                     else $scope.sensibilidad.fase = res;
                     $scope.cambioSenbilidad(tipo);*/
                    $scope.modulacion.sensibilidad = res;
                    // $scope.modularForm.$dirty = true;
                    $scope.validarPeticion($scope.modularForm, true); //Otro parchaso
                });
        };

        $scope.changeFrecuencia = function (senal) {
            var frecuenciaDef, frecuenciaAux;
            if (senal == "moduladora") {
                frecuenciaDef = $scope.modulacion.moduladora;
                frecuenciaAux = $scope.frecuenciaModuladora;
            }
            else {
                frecuenciaDef = $scope.modulacion.portadora;
                frecuenciaAux = $scope.frecuenciaPortadora;
            }
            if (frecuenciaAux.valor > 0 && frecuenciaAux.unidad.valor > 0) {
                frecuenciaDef.frecuencia = frecuenciaAux.valor * frecuenciaAux.unidad.valor;
            }
        };

        function getSenal(senalChart, data) {
            senalChart.loading = true;
            $http.post(API_URL + '/signal', data)
                .success(
                    function (response) {
                        senalChart.series[0].data = response.puntos;
                        senalChart.xAxis.max = response.max;
                        senalChart.loading = false;
                        senalChart.title.text = response.modelo;
                        // Con esto activo el tooltip, solo entrará la primera vez
                        if (!senalChart.options.tooltip.enabled)
                            senalChart.options.tooltip.enabled = true;
                    })
                .error(
                    function (error) {
                        console.log(error);
                    }
                );
        }

        function limitarDecimales(num, max = 5) {
            var resul = num.toString().split(".");
            if (resul.length > 1)
                return num.toLocaleString('en-US', {
                    minimumFractionDigits: 0,
                    maximumFractionDigits: max
                });
            return num;
        }

        function modularSenal (solo_puntos = false) {
            $scope.loadingModuladora = true;
            $scope.moduladaChartConfig.loading = true;
            $scope.espectrosChartConfig.loading = !solo_puntos;
            var data = angular.copy($scope.modulacion);
            data.ruido = $scope.ruido.val;
            data.solo_puntos = solo_puntos;
            $http.post(API_URL + '/modulada', data)
                .success(function (response) {
                    $scope.moduladaChartConfig.series[0].data = response.puntos;
                    $scope.moduladaChartConfig.xAxis.max = response.max;
                    if(!solo_puntos){
                        $scope.moduladaChartConfig.title.text = response.modelo;
                        $scope.modulacion.modulada = {
                            indice: parseFloat(limitarDecimales(response.m, 8)),
                            anchoBessel: parseFloat(limitarDecimales(response.b_bessel, 4)),
                            anchoCarson: parseFloat(limitarDecimales(response.b_carson, 4)),
                            potenciaTotal: parseFloat(limitarDecimales(response.potencia_total)),
                            potenciaPromedio: parseFloat(limitarDecimales(response.potencia_promedio, 3)),
                            desMax: $scope.modulacion.sensibilidad * $scope.modulacion.moduladora.amplitud,
                            n: response.n
                        };
                        //seteo de el espectro de frecuencias
                        primero = angular.copy(response.espectros[0]);
                        var espectro_central = {
                            x : primero.f,
                            y : primero.v,
                            potencia: primero.p,
                            polaridad: primero.polaridad
                        };
                        var espectros = [];
                        espectros.push(espectro_central);
                        espectrosig = angular.copy(response.espectros);
                        espectrosig.shift();
                        angular.forEach(espectrosig, function(obj){
                            espectros.unshift({x : obj.fneg, y : obj.v, potencia: obj.p, polaridad: obj.polaridad});
                            espectros.push({x : obj.fpos, y : obj.v, potencia: obj.p, polaridad: obj.polaridad});
                        });
                        $scope.espectrosChartConfig.series[0].data = espectros;
                        $scope.espectrosChartConfig.loading = false;
                    }
                    // Hablito opciones de onda modulada
                    if ($scope.$parent.noModulado)
                        $scope.$parent.noModulado = false;

                    $scope.loadingModuladora = false;
                    $scope.moduladaChartConfig.loading = false;
                    $scope.moduladaChartConfig.options.tooltip.enabled = true;
                    //"demodulo" la señal
                    var moduladora = $scope.modulacion.moduladora;
                    var data = {
                        amplitud: moduladora.amplitud,
                        frecuencia: moduladora.frecuencia,
                        modelo: moduladora.modelo,
                        tipo: "d",
                        ruido : $scope.ruido.val
                    };
                    getSenal($scope.demoduladaChartConfig, data);
                });
        }

        $scope.$watch("ruido.val", function(nuevo, viejo){
            //watchet -> CHIMBO VALE XD
            if(nuevo != viejo){
                var data= {};
                if ($scope.moduladoraForm.$valid){
                    var moduladora = $scope.modulacion.moduladora;
                    data = {
                        amplitud: moduladora.amplitud,
                        frecuencia: moduladora.frecuencia,
                        modelo: moduladora.modelo,
                        tipo: "m",
                        ruido : $scope.ruido.val
                    };
                    getSenal($scope.moduladoraChartConfig, data);
                }
                if ($scope.portadoraForm.$valid){
                    var portadora = $scope.modulacion.portadora;
                    data = {
                        amplitud: portadora.amplitud,
                        frecuencia: portadora.frecuencia,
                        modelo: portadora.modelo,
                        tipo: "p",
                        ruido : $scope.ruido.val
                    };
                    getSenal($scope.portadoraChartConfig, data);
                }
                if ($scope.moduladoraForm.$valid && $scope.portadoraForm.$valid && $scope.modularForm.$valid) {
                    modularSenal(true);
                }
            }

        });

        $scope.calcInstantanea = function (t, desviacion) {
            data = {
                sensibilidad: $scope.modulacion.sensibilidad,
                moduladora: $scope.modulacion.moduladora,
                t: t
            };

            if (!desviacion) {
                data.frec_portadora = $scope.modulacion.portadora.frecuencia;
                data.tipo = $scope.modulacion.tipo;
            }

            $http.post(API_URL + '/instantanea', data)
                .success(
                    function (response) {
                        // result = limitarDecimales(parseFloat(response));
                        if (!desviacion) $scope.modulacion.modulada.fInst = response;
                        else $scope.modulacion.modulada.desInst = response;
                    })
                .error(
                    function (error) {
                        console.log(error);
                    }
                );
        };

        $scope.cambioIndice = function () {
            var moduladora = $scope.modulacion.moduladora;
            var modulada = $scope.modulacion.modulada;
            if ($scope.modulacion.tipo == "FM") {
                $scope.modulacion.sensibilidad = (modulada.indice * 2*Math.PI*moduladora.frecuencia)/(moduladora.amplitud);
            }
            else {
                $scope.modulacion.sensibilidad = modulada.indice / moduladora.amplitud;
            }
            // $scope.cambioSenbilidad($scope.modulacion.tipo);
            $scope.validarPeticion($scope.moduladoraForm, true);
        };

        $scope.cambioDesv = function () {
            var moduladora = $scope.modulacion.moduladora;
            var modulada = $scope.modulacion.modulada;
            if ($scope.modulacion.tipo == "FM") {
                $scope.modulacion.sensibilidad = (modulada.desMax)/(moduladora.amplitud);
                // $scope.cambioSenbilidad($scope.modulacion.tipo);
                $scope.validarPeticion($scope.moduladoraForm, true);
            }
        }

    }])
    .controller('DesviacionDialogCtrl', [ '$scope', '$mdDialog', 'tipo', function ($scope, $mdDialog, tipo) {
        $scope.tipo = tipo;
        if (tipo == 'FM')
            $scope.titulo = 'frecuencia';
        else
            $scope.titulo = 'fase';

        $scope.cancel = function() {
            $mdDialog.cancel();
        };

        $scope.calcular = function() {
            // var sensibilidad = $scope.desviacion1 / $scope.desviacion2;
            $mdDialog.hide($scope.desviacion1 / $scope.desviacion2);
        };
    }]);