/**
 * Created by Jefferson on 1/2/2017.
 */
angular.module('lautcom')
    .controller('HomeCtrl', ['$scope', '$location', '$anchorScroll', 'lodash', '$document',
        function ($scope, $location, $anchorScroll, lodash, $document) {
            var originatorEv;
            $scope.ruido = {val : false};
            $scope.noModulado = true;
            $scope.openMenu = function($mdOpenMenu, ev) {
                originatorEv = ev;
                $mdOpenMenu(ev);
            };
            $scope.goTo = function (id) {
                // $location.hash(id);
                var elemento = angular.element(document.getElementById(id));
                $document.duScrollTo(elemento, 80, 600);
                // $anchorScroll();
            };
            if (!lodash.isEmpty($location.hash())) {
                $scope.currentNavItem = $location.hash();
            }
        }])
    .value('duScrollSpy', true);