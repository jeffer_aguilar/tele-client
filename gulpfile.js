/**
 * Created by Jefferson on 10/10/2016.
 */
'use strict';

var gulp = require('gulp'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    watch = require('gulp-watch'),
    livereload = require('gulp-livereload'),
    connect = require('gulp-connect'),
    inject = require('gulp-inject'),
    mainBowerFiles = require('main-bower-files');

gulp.task('minificado', function () {
    return gulp.src('./scripts/**/*.js')
        .pipe(concat('compilacion.js'))
        .pipe(uglify())
        .pipe(gulp.dest('./bundle'))
});

/********************************************
 *****************GULP INJECT****************
 ********************************************/

gulp.task('indexJs', function () {
    var target = gulp.src('./index.html');
    // It's not necessary to read the files (will speed up things), we're only after their paths:
    var sources = gulp.src(['./scripts/**/*.js', './components/**/*.js'], {read: false});

    return target.pipe(inject(sources))
        .pipe(gulp.dest('./'));
});

/*gulp.task('indexBower', function () {
    return gulp.src('./index.html')
        .pipe(inject(gulp.src(mainBowerFiles(), {read: false}), {name: 'bower', relative: true}))
        .pipe(gulp.dest('./'));
});*/

/********************************************
 ***************LIVE RELOAD******************
 ********************************************/

gulp.task('serve', function() {
    connect.server({
        root: './',
        port: 9000,
        livereload: true
    });
});

gulp.task('html', function () {
    gulp.src('./**/*.html')
        .pipe(connect.reload());
});

gulp.task('js', function () {
    gulp.src(['./scripts/**/*.js', './components/**/*.js'])
        .pipe(connect.reload());
});

/*********************************************
 ****************** WATCHERS******************
 *********************************************/

gulp.task('watch', function () {
    gulp.watch(['./**/*.html'], ['html']);
    gulp.watch(['./scripts/**/*.js'], ['js']);
    gulp.watch(['./scripts/**/*.js', './components/**/*.js'], ['indexJs']);
});

gulp.task('default', ['serve', 'watch', 'indexJs']);
